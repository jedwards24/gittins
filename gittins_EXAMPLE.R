########################
# Example code to produce Gittins indices.
########################

source("gittins_FUNC.R")

################
# Calculates Gittins indices needed for a BMAB with Beta(1, 1) priors and 20 actions.
# The (a, b) entry in the returned matrix corresponds to the index for a state with alpha=a and beta=b.
# Indices are only calculated for states with alpha + beta <= num_actions + 1.
################
N <- 80
alpha <- 1
beta <- 1
gamma <- 0.9
tol <- 5e-5
num_actions <- 20 

gi_matrix_ab <- bmab_gi_multiple_ab(alpha_start=alpha, beta_start=beta, gamma, N, num_actions, tol, kgi=T)

#The following does the same with the Sigma, n parameterisation. The resulting matrix is arranged differently.
gi_matrix <- bmab_gi_multiple(Sigma_start=alpha, n_start=alpha + beta, gamma, N, num_actions, tol, kgi=T)

################
# Calculates Gittins indices for an NMAB with n = 1, 2, ..., 20.
################
n_range <- 1 : 20 #must be ascending
tau <- 1
gamma <- 0.9
N <- 30
xi <- 3
delta <- 0.02
tol <- 5e-5

gi_vec <- nmab_gi_multiple(n_range, gamma, tau, tol, N, xi, delta)
gi_vec

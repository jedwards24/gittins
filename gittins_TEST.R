###############
#Runs unit tests. The files for these are in the subfolder "tests". 

#Some Notes on testthat
#-----------------------
#Any variables set inside a test_that() function call are local.
#When using test_dir(path) the working directory (wd) is set to the argument "path
#for the duration of the test.
#I can change the wd within test files but after the file is finished the wd goes
# back to "path" and after all tests are finished the wd goes back to what it was before the
# test_dir() call.

library("testthat")
source("gittins_FUNC.R")

test_dir("tests/")

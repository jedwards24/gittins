# README for Gittins project

Author: James Edwards   
j.edwards4@lancaster.ac.uk  
First distributed October 2016.  
Available from https://bitbucket.org/jedwards24/gittins  
License: MIT + file LICENSE

**This project has moved to https://github.com/jedwards24/gittins which holds a more recent version as an R package.**

## DESCRIPTION

This project contains functions written in R to calculate Gittins indices for the Bayesian multi-armed bandit problem with Bernoulli or Normal rewards. More information on the methodology can be found in my thesis at http://eprints.lancs.ac.uk/84589/ (in Chapter 5).

## CONTENTS

+ The file gittins_FUNC.R contains all functions.
+ The file gittins_EXAMPLE.R contains example code and settings.
+ To download both go to "source" on the BitBucket page (https://bitbucket.org/jedwards24/gittins/src)
+ Save both files to the same location and run the code in `gittins_EXAMPLE.R` 

## DETAILS OF FUNCTIONS

Gittins indices are calculated using calibration which requires repeated solving of a one-armed bandit stopping problem. The functions that do this will not normally need to be called directly. These functions include:
`calibrate_arm(...)` - performs the main calibration using an appropriate value function.
Value functions e.g. `calc_value_gi_bmab(...)` - calculate the value of a state in the one-armed bandit.
Bounding functions - used to give initial bounds for Gittins indices. Upper bounds are given by GI+ (giplus in function names) while lower bounds use the knowledge gradient index (KGI).

The remaining functions calculate the indices. The first group of these do so for a single state at a time:

+ `calc_gi_bmab(Sigma, n, gamma, tol, N, lb=NA, ub=NA, kgi=T, giplus=T)`
+ `calc_gi_bmab_ab(a, b, gamma, tol, N, lb=NA, ub=NA, kgi=T, giplus=T)`
+ `calc_gi_nmab(Sigma, n, gamma, tau, tol, N, xi, delta, lb=NA, ub=NA, kgi=T, giplus=T)`

The first two are for the Bernoulli multi-armed bandit (BMAB) while the last is for the Normal multi-armed bandit (NMAB). The arguments are as follows:

+ `Sigma` and `n` - parameters for the arm representing respectively, the Bayesian sum of observations and number of observations.
+ `a` and `b` - alternative and more common parameterisation of an arm where `a = Sigma` and `b = n - Sigma`.
+ `gamma` - discount factor where 0 < `gamma` < 1.
+ `tol` - the tolerance or accuracy to which the index is calculated.
+ `N` - length of horizon in the one-armed bandit. This produces an approximation which underestimates the index value. Accuracy can be guaranteed by using a large enough `N`. In practice for the BMAB `N=80` and `N=700`, respectively, are high enough for 4 d.p. accuracy for `gamma=0.9` and `gamma=0.99`. For NMAB `N=40` and `N=200` respectively are sufficient for the same accuracy.
+ `xi` and `delta` - the NMAB solution to the one-armed bandit involves a discrete state space approximation which is controlled by `xi` and `delta`. The extent of the space is controlled by `xi` and `delta` controls the fineness. Smaller `delta` and larger `xi` therefore increase accuracy but increase computation time. For `gamma <= 0.99` `xi = 3` is sufficient for high accuracy. Approximation error due to delta is hard to avoid but `delta=0.01` gives high accuracy for `gamma = 0.9` and is good enough for at least 3 d.p. accuracy for `gamma=0.99`.

The last four arguments control initial bounds. The defaults will work well.

The last three functions calculate indices for a number of states at once:

+ `bmab_gi_multiple(Sigma_start=1, n_start=2, gamma, N, num_actions, tol, kgi=T)`
+ `bmab_gi_multiple_ab(alpha_start=1, beta_start=1, gamma, N, num_actions, tol, kgi=T)`
+ `nmab_gi_multiple(n_range, gamma, tau, tol, N, xi, delta)`

Their use is described in comments with the functions in `gittins_FUNC.R` and in `gittins_EXAMPLE.R`.

